package com.teamarch.fg;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class MainActivity extends Activity {
	
	Bundle bundle;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		Log.i("fg splash", "MainActivity.onCreate()");
		
		Thread timer = new Thread(new Runnable() {

			@Override
			public void run() {
				// TODO Auto-generated method stub
				try {
					Log.i("fg splash", "Will move to UserModeSelect in 2.5 seconds");
					Thread.sleep(2500);
				} catch(Exception e) {
					e.printStackTrace();
				} finally {
					Intent intent = new Intent("com.teamarch.fg.USERMODESELECT");
					startActivity(intent);
				}
			}
			
		});
		
		timer.start();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		finish();
	}
	

}
