package com.teamarch.fg;

public enum UserMode {
	REGULAR,
	MASTER,
	UNDEFINED;
}
