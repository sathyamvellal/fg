package com.teamarch.fg;

import com.intel.stc.utility.StcUser;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.ListView;
import android.widget.TextView;

public class SelectGalleryActivity extends AbstractServiceUsingActivity
		implements IFloatingGalleryEventListener {

	Dialog dialog;
	Bundle bundle;
	Handler myHandler = new Handler();
	GalleryAdapter galleryAdapter;
	ListView lview;
	TextView tview;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		Log.i("fg gallery_activity", "SelectGalleryActivity.onCreate()");
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		this.bundle = this.getIntent().getExtras();

		setContentView(R.layout.select_gallery);
		lview = (ListView) findViewById(R.id.galleryListView);
		tview = (TextView) findViewById(R.id.txtGalleryOnline);

		doStartService();
	}

	@Override
	protected void onServiceConnected() {
		Log.i("fg gallery_activity",
				"SelectGalleryActivity.onServiceConnected()");
		// TODO Auto-generated method stub
		galleryAdapter = new GalleryAdapter(fgService, this);
		fgService.addListener(this);
		fgService.parseInitBundle(bundle);

		myHandler.post(new Runnable() {
			public void run() {
				ListView lview = (ListView) findViewById(R.id.galleryListView);
				lview.setAdapter(galleryAdapter);
			}
		});
	}

	@Override
	protected void onServiceDisconnected() {
		Log.i("fg gallery_activity",
				"SelectGalleryActivity.onServiceDisconnected()");
		// TODO Auto-generated method stub
		fgService.removeListener(this);
	}

	public void sendInvitation(StcUser user) {
		// TODO Auto-generated method stub
		if (fgService.sendInvitation(user)) {
			//dialog = ProgressDialog.show(this, "", "Waiting for connection");
		}
	}

	@Override
	public void dataReceived(byte[] header) {
		// TODO Auto-generated method stub

	}

	@Override
	public void remoteDisconnect() {
		// TODO Auto-generated method stub

	}

	public void connected(final boolean didConnect) {
		Log.i("fg gallery_activity", "SelectGalleryActivity.connected()");
		myHandler.post(new Runnable() {
			public void run() {
				if (didConnect) {
					Log.i("fg gallery_activity",
							"Connection Successful, jumping to gallery!");
					Intent intent = new Intent(SelectGalleryActivity.this,
							TempGalleryActivity.class);
					startActivity(intent);
					finish();
				} else {
					Log.i("fg gallery_activity",
							"connection failed, shutting down.");
					doStopService();
					finish();
				}
			}
		});
	}

}
