package com.teamarch.fg;

import android.os.Bundle;

public class MasterSettingsActivity extends AbstractServiceUsingActivity {

	Bundle bundle;
	
	@Override
	protected void onServiceConnected() {
		// TODO Auto-generated method stub
		fgService.addListener(this);
		fgService.parseInitBundle(bundle);
	}

	@Override
	protected void onServiceDisconnected() {
		// TODO Auto-generated method stub
		fgService.removeListener(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.master_settings);
		//fgService.fooLog("Hello from MasterSettingsActivity");
	}

	@Override
	public void dataReceived(byte[] header) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remoteDisconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connected(boolean connected) {
		// TODO Auto-generated method stub
		
	}

}
