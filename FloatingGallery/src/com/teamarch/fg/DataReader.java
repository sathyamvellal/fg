package com.teamarch.fg;

import android.annotation.TargetApi;
import android.os.Build;
import android.util.Log;

import java.net.IDN;
import java.util.HashMap;

@TargetApi(Build.VERSION_CODES.GINGERBREAD)
public class DataReader {
	HashMap<Integer, String> bufferMap = new HashMap<Integer, String>();
	HashMap<Integer, Data> dataMap = new HashMap<Integer, Data>();
	ReadEngine rengine;
	byte[] image;
	
	DataReader(ReadEngine rengine) {
		Log.i("fg data_reader", "Initializing DataReader");
		this.rengine = rengine;
	}
	
	void add(byte[] buffer) {
		Log.i("fg data_reader", "got a buffer!! check now");
		Log.i("fg data_reader", "" + buffer[0]);
		Log.i("fg data_reader", "" + buffer[2]);
		int id = buffer[1];
		String s = bufferMap.get(id);
		if (s == null || s.isEmpty()) {
			Log.i("fg data_reader", "Slot was empty. Placing new object");
			bufferMap.put(id, new String(buffer).substring(4));
		} else {
			Log.i("fg data_reader", "Adding more data");
			bufferMap.put(id, bufferMap.get(id) + new String(buffer).substring(4));
		}
		Log.i("fg data_reader", "Type of data received: " + Data.getTypeString(buffer[0]));
		image = buffer;
		Log.i("fg data_reader", "Look at this! : " + new String(image));
		rengine.gotData(buffer);
//		if ((int) buffer[2] == 1) {
//			Log.i("fg data_reader", "Finalizing data!!!!");
////			this.putData(id, buffer);
////			dataMap.put(id, new TextData(bufferMap.remove(id)));
//			Log.i("fg data_reader", "Removed and put in dataMap");// + dataMap.get(id).getData());
////			rengine.gotData(buffer);
//			image = buffer;
//			Log.i("fg data_reader", "Look at this! : " + image);
//		}
	}
	
	void putData(int id, byte[] header) {
		final String type = Data.getTypeString(header[0]);
		Log.i("fg data_reader", "Putting data of type : " + type);
		
		switch ((int)header[0]) {
		case 50:
			Log.i("fg data_reader", "Adding the data now");
			dataMap.put(id, new TextData(bufferMap.remove(id)));
			break;
		case 99:
			Log.i("fg data_reader", "Adding the data now");
			dataMap.put(id, new TextData(bufferMap.remove(id)));
		case 100:
			Log.i("fg data_reader", "Adding the image data now! Woooo");
			dataMap.put(id, new ImageData(bufferMap.remove(id)));
			break;
		}
	}
	
	abstract static class Data {
		byte[] dataStream;
		
		static String getTypeString(byte type) {
			String typeString = null;
			
			switch ((int)type) {
			case 10:
				//master/regular query
				typeString = "mr_query";
				break;
			case 11:
				//master/regular response
				typeString = "mr_response";
				break;
			case 15:
				//user list query
				typeString = "ul_query";
				break;
			case 16:
				//user list response
				typeString = "ul_response";
				break;
			case 20:
				//data length packet
				typeString = "data_length";
				break;
			case 50:
				//text data
				typeString = "text_data";
				break;
			case 99:
				//image request
				typeString = "image_request";
				break;
			case 100:
				//image data
				typeString = "image_data";
				break;
			default:
				return "";
			}
			
			return typeString;
		}
		
		abstract byte[] getData();
	}
	
	class TextData extends Data {
		byte[] getData() {
//			return new String(this.dataStream);
			return this.dataStream;
		}
		
		TextData() {
		}
		
		TextData(String msg) {
			this.dataStream = msg.getBytes();
		}
		
	}
	
	class ImageData extends Data {
		@Override
		byte[] getData() {
			// TODO Auto-generated method stub
			return this.dataStream;
		}
		
		ImageData() {
		}
		
		ImageData(String msg) {
//			if (msg != null) {
				this.dataStream = msg.getBytes();
//			} else {
//				Log.i("fg data_reader", "This is not working. Msg is null! Avoided an exception now!");
//			}
		}
	}

	public byte[] getData(byte[] header) {
		// TODO Auto-generated method stub
		Log.i("fg data_reader", "getting data.. muahahahahaha");
		int id = header[1];
		Data d = dataMap.remove(id);
//		return d.getData();
		return image;
	}

	public void addImage(byte[] buffer) {
		// TODO Auto-generated method stub
		rengine.gotData(buffer);
	}
}
