package com.teamarch.fg;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

public class TempGalleryActivity extends AbstractServiceUsingActivity implements
		IFloatingGalleryEventListener {

	Handler myHandler = new Handler();
	Bundle bundle;
	Button b;
	EditText et;
	TextView tv;
	ImageView iv;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.temp_gallery);
		b = (Button) findViewById(R.id.submit);
		et = (EditText) findViewById(R.id.textInput);
		tv = (TextView) findViewById(R.id.textView1);
		tv.setText(new String("Chat: \n"));
		iv = (ImageView) findViewById(R.id.imageView);
		
		b.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				fgService.writeString("" + et.getText().toString());
//				tv.append("Me : " + et.getText().toString() + "\n");
//				et.setText("");
			}
		});
		
		doStartService();
	}

	@Override
	public void dataReceived(final byte[] buffer) {
		// TODO Auto-generated method stub
//		final String str = fgService.getLines();
		final byte[] str = fgService.getData(buffer);
		Log.i("fg temp_gallery", "Line received!!" + str);
		final int type = (int) buffer[0];
		myHandler.post(new Runnable() {
			public void run() {
//				if (type == 99) {
//					Log.i("fg temp_gallery", "Request received for " + str);
//				}
//				tv.setText("Him: " + str + "\n");
//				Bitmap bm = BitmapFactory.decodeFile(str);
//				Log.i("fg temp_gallery", "File Path: " + str);
//				Log.i("fg temp_gallery", "Requesting for the above mentioned file path");
//				fgService.requestImage(str);
//				iv.setImageBitmap(bm);
				Log.i("fg temp_gallery", "WoohooO!!!!");
				Log.i("fg temp_gallery", "str: " + (str == null) + " + " + str);
				Bitmap bm = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
				iv.setImageBitmap(bm);
			}
		});
	}

	@Override
	public void remoteDisconnect() {
		// TODO Auto-generated method stub

	}

	@Override
	protected void onServiceConnected() {
		// TODO Auto-generated method stub
		Log.i("fg temp_gallery", TempGalleryActivity.this.getClass().getCanonicalName() + "onServiceConntected()");
		fgService.addListener(this);
	}

	@Override
	protected void onServiceDisconnected() {
		// TODO Auto-generated method stub
		fgService.removeListener(this);
	}

	@Override
	public void connected(boolean connected) {
		// TODO Auto-generated method stub

	}

}
