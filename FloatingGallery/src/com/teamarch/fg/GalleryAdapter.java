package com.teamarch.fg;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.Collections;

import com.intel.stc.utility.StcUser;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GalleryAdapter extends BaseAdapter {
	
	Dialog upDialog;
	FloatingGalleryService fgService;
	SelectGalleryActivity selectActivity;
	public List<StcUser> galleryList = new ArrayList<StcUser>();
	
	GalleryAdapter(FloatingGalleryService fgService, SelectGalleryActivity selectActivity) {
		Log.i("fg gallery_adapter", "GalleryAdapter.GalleryAdapter()");
		this.fgService = fgService;
		this.selectActivity = selectActivity;
		stcNewUserList(fgService.getUsers());
	}
	
	public void stcNewUserList(List<StcUser> newList) {
		Log.i("fg gallery_adapter", "GalleryAdapter.stcNewUserList()");
		synchronized(galleryList) {
			if (newList.size() == 0) {
				galleryList = newList;
			} else {
				for (int i = newList.size() - 1; i >= 0; --i) {
					StcUser user = newList.get(i);
					if (!user.isAvailable()) {
						newList.remove(i);
					} else {
						UUID userApps[] = user.getAppList();
						boolean found = false;
						for (UUID userApp : userApps) {
							if (userApp.toString().compareToIgnoreCase(FloatingGalleryRegisterApp.APP_UUID) == 0) {
								found = true;
								break;
							}
						}
						if (!found) {
							newList.remove(i);
						}
					}
				}
				galleryList = newList;
				Collections.sort(galleryList);
			}
			selectActivity.tview.setText("" + galleryList.size() + " Online");
		}
	}

	@Override
	public int getCount() {
		Log.i("fg gallery_adapter", "GalleryAdapter.getCount()");
		// TODO Auto-generated method stub
		synchronized(galleryList) {
			return galleryList.size();
		}
	}

	@Override
	public Object getItem(int position) {
		Log.i("fg gallery_adapter", "GalleryAdapter.getItem()");
		// TODO Auto-generated method stub
		synchronized(galleryList) {
			if (galleryList != null && position < galleryList.size() && position >= 0) {
				return galleryList.get(position);
			} else {
				return null;
			}
		}
	}

	@Override
	public long getItemId(int position) {
		Log.i("fg gallery_adapter", "GalleryAdapter.getItemId()");
		// TODO Auto-generated method stub
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Log.i("fg gallery_adapter", "GalleryAdapter.getView()");
		// TODO Auto-generated method stub
		if (convertView == null) {
			LayoutInflater inflater = (LayoutInflater)fgService.getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = inflater.inflate(R.layout.gallery_row, null);
		} else {
			convertView.setVisibility(View.VISIBLE);
		}
		
		StcUser curUser = null;
		synchronized(galleryList) {
			if (position >= 0 && position < galleryList.size()) {
				curUser = (StcUser)getItem(position);
			}
		}
		
		if (curUser == null) {
			convertView.setVisibility(View.GONE);
			return convertView;
		}
		
		ImageView avatar = (ImageView)convertView.findViewById(R.id.row_userAvatar);
		if (curUser.getAvatar() == null) {
			avatar.setImageResource(R.drawable.generic_avatar);
		} else {
			avatar.setImageBitmap(curUser.getAvatar());
		}
		
		TextView userName = (TextView)convertView.findViewById(R.id.row_userName);
		userName.setText(curUser.getUserName());
		
		final StcUser user = curUser;
		convertView.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
//				selectActivity.inviteUser(user);
//				selectActivity.openGallery(user);
				selectActivity.sendInvitation(user);
			}
		});
		return convertView;
	}

}
