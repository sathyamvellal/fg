package com.teamarch.fg;

public interface IServiceIOListener {
	void dataReceived(byte[] header);
	void remoteDisconnect();
}
