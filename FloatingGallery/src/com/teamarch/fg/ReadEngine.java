package com.teamarch.fg;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import android.net.Uri;
import android.util.Log;
import android.widget.ImageView;

public class ReadEngine implements Runnable {

	IServiceIOListener listener;
	InputStream iStream;
	ArrayList<String> textList = new ArrayList<String>();
	Thread thread;
	String myString = new String();
	volatile boolean running = true;
	DataReader dataReader;

	ReadEngine(InputStream iStream, IServiceIOListener listener) {
		Log.i("fg read_engine", "Constructing a ReadEngine");
		this.iStream = iStream;
		this.listener = listener;
		this.dataReader = new DataReader(this);
		thread = new Thread(this);
		thread.setDaemon(true);
		thread.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			InputStreamReader reader = new InputStreamReader(iStream);
			StringBuffer sb = new StringBuffer();
			while (running) {
//				char[] buffer = new char[1024];
				byte[] bytes = new byte[10240];
				Log.i("fg read_engine", "Waiting for data!");
//				int numBytes = reader.read(buffer);
				int numBytes = iStream.read(bytes);
				Log.i("fg read_engine", "read line!! " + new String(bytes));
				dataReader.add(bytes);
				if (numBytes < 0) {
					running = false;
					listener.remoteDisconnect();
				} else {
//					sb.append(buffer, 0, numBytes);
					addLine(sb);
					sb = new StringBuffer();
				}
			}
		} catch (UnsupportedEncodingException e) {
		} catch (IOException e) {
			listener.remoteDisconnect();
			running = false;
		}
	}

	private void addLine(StringBuffer sb) {
		// TODO Auto-generated method stub
//		int count;
//		synchronized(textList) {
//			textList.add(new String(sb.toString()));
//			myString = new String(myString + "Him: " + line + "\n");
//		}
//		listener.lineReceived(1);
	}
	
	void gotData(byte[] header) {
		listener.dataReceived(header);
	}

	public String getLines() {
		// TODO Auto-generated method stub
		synchronized(textList) {
			if (textList.size() > 0) {
				Log.i("fg read_engine", ">" + textList.size());
				return textList.remove(0);
			} else {
				Log.i("fg read_engine", "<" + textList.size());
				return "..\n";
			}
		}
	}

	public byte[] getData(byte[] header) {
		// TODO Auto-generated method stub
		return dataReader.getData(header);
	}
}
