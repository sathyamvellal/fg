package com.teamarch.fg;

import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import android.util.Log;

public class WriteEngine implements Runnable {

	IServiceIOListener listener;
	OutputStream oStream;
	BlockingQueue<String> queue;
	Thread thread;
	volatile boolean running = true;
	DataWriter dataWriter;

	WriteEngine(IServiceIOListener listener, OutputStream oStream) {
		this.listener = listener;
		this.oStream = oStream;
		queue = new LinkedBlockingQueue<String>();
		dataWriter = new DataWriter(oStream);
		thread = new Thread(this);
		thread.setDaemon(true);
		thread.setName("Write Engine Thread");
		thread.start();
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		Log.i("sunil", "WriteEngine.run()");
		while (running) {
			try { 
				String s = queue.take();
				dataWriter.addImage(s);
				//if (running) {
				//	oStream.write(s.getBytes());
				//}
			} catch (InterruptedException e) {
				Log.e("fg write_engine", e.toString());
				listener.remoteDisconnect();
			}// catch (IOException e) {
			//	Log.i("fg write_engine", e.toString());
			//	listener.remoteDisconnect();
			//}
		}
	}

	public void writeString(String s) {
		Log.i("fg write_engine", "WriteEngine.writeString()");
		queue.add(s);
	}

	public void stop() {
		Log.i("fg write_engine", "WriteEngine.stop()");
		running = false;
		queue.add(" ");
	}

	public void requestImage(String str) {
		// TODO Auto-generated method stub
		Log.i("fg temp_gallery", "Forwarding request");
		dataWriter.requestImage(str);
	}
}
