package com.teamarch.fg;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

public class UserModeSelect extends AbstractServiceUsingActivity {
	
	Button host;
	Button join;
	
	Bundle bundle;

	@Override
	protected void onServiceConnected() {
		// TODO Auto-generated method stub
		fgService.addListener(this);
		fgService.parseInitBundle(bundle);
	}

	@Override
	protected void onServiceDisconnected() {
		// TODO Auto-generated method stub
		fgService.removeListener(this);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.user_mode);
		Log.i("fg user_mode", "UserModeSelect.onCreate()");
		initialize();
		
		host.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fgService.userMode = UserMode.MASTER;
				Intent intent = new Intent("com.teamarch.fg.MASTERSETTINGS");
//				Intent intent = new Intent(UserModeSelect.this, MasterSettingsActivity.class);
				startActivity(intent);
			}
		});
		
		join.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				fgService.userMode = UserMode.REGULAR;
				Intent intent = new Intent("com.teamarch.fg.SELECTGALLERY");
//				Intent intent = new Intent(UserModeSelect.this, SelectGalleryActivity.class);
				startActivity(intent);
			}
		});
		
		doStartService();
	}

	private void initialize() {
		// TODO Auto-generated method stub
		host = (Button) findViewById(R.id.btn_userModeHost);
		join = (Button) findViewById(R.id.btn_userModeJoin);
	}

	@Override
	public void dataReceived(byte[] header) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void remoteDisconnect() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void connected(boolean connected) {
		// TODO Auto-generated method stub
		
	}

}
