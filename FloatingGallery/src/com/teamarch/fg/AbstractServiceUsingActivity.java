package com.teamarch.fg;

import com.teamarch.fg.AbstractServiceUsingActivity;
import com.teamarch.fg.FloatingGalleryService;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;

public abstract class AbstractServiceUsingActivity extends Activity implements
		IFloatingGalleryEventListener {

	abstract protected void onServiceConnected();

	abstract protected void onServiceDisconnected();

	protected FloatingGalleryService fgService;
	private FloatingGalleryServiceConnection mConnection = new FloatingGalleryServiceConnection(
			this);
	boolean isBound = false;

	@Override
	protected void onResume() {
		Log.i("fg abstract", "AbstractServiceUsingActivity.onResume()");
		doBindService();
		super.onResume();
	}

	@Override
	protected void onPause() {
		Log.i("fg abstract", "AbstractServiceUsingActivity.onPause()");
		doUnbindService();
		super.onPause();
	}

	private void doBindService() {
		if (!isBound) {
			Log.i("fg abstract", "binding service " + mConnection.toString());
			Intent servIntent = new Intent(
					"com.teamarch.fg.FloatingGalleryService");
			isBound = bindService(servIntent, mConnection, 0);
			if (!isBound)
				Log.i("fg abstract", "service did not bind.");
		}
	}

	private void doUnbindService() {
		if (isBound) {
			Log.i("fg abstract", "AbstractServiceUsingActivity.doUnbindService()");
			isBound = false;
			unbindService(mConnection);
		}
	}

	protected void doStartService() {
		Log.i("fg abstract", "AbstractServiceUsingActivity.doStartService()");
		Intent servIntent = new Intent("com.teamarch.fg.FloatingGalleryService");
		startService(servIntent);
	}

	protected void doStopService() {
		Log.i("fg abstract", "AbstractServiceUsingActivity.doStopService()");
		Intent servIntent = new Intent("com.teamarch.fg.FloatingGalleryService");

		mConnection.serviceExited();
		doUnbindService();
		stopService(servIntent);
	}

	public class FloatingGalleryServiceConnection implements ServiceConnection {
		AbstractServiceUsingActivity activity;
		boolean serviceStopped = false;

		FloatingGalleryServiceConnection(AbstractServiceUsingActivity activity) {
			this.activity = activity;
		}

		public void onServiceConnected(ComponentName className, IBinder binder) {
			synchronized (this) {
				Log.i("fg abstract connection", "FloatingGalleryServiceConnection.onSeriveConnected()");

					fgService = (FloatingGalleryService) ((FloatingGalleryService.StcServInetBinder) binder)
						.getService();
				if (serviceStopped)
					fgService.exitService();
				else
					activity.onServiceConnected();
			}
		}

		public void onServiceDisconnected(ComponentName className) {
			Log.i("fg abstract connection", "FloatingGalleryServiceConnection.onSeriveDisconnected()");

			activity.onServiceDisconnected();
			fgService = null;
		}

		public void serviceExited() {
			Log.i("fg abstract connection", "FloatingGalleryServiceConnection.onSeriveExited()");
			synchronized (this) {
				serviceStopped = true;
			}
		}
	};

}
