package com.teamarch.fg;

//import java.io.InputStream;
//import java.io.OutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

//import android.app.Dialog;
//import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.teamarch.fg.IServiceIOListener;
import com.teamarch.fg.ReadEngine;
import com.intel.stc.events.InviteRequestEvent;
import com.intel.stc.events.InviteResponseEvent;
import com.intel.stc.events.StcException;
import com.intel.stc.events.StcUpdateEvent;
import com.intel.stc.interfaces.IStcActivity;
import com.intel.stc.interfaces.StcConnectionListener;
import com.intel.stc.interfaces.StcUserListListener;
import com.intel.stc.lib.StcLib;
import com.intel.stc.slib.IStcServInetClient;
import com.intel.stc.slib.StcServiceInet;
import com.intel.stc.utility.StcApplicationId;
import com.intel.stc.utility.StcSocket;
import com.intel.stc.utility.StcUser;

public class FloatingGalleryService extends StcServiceInet implements
		StcUserListListener, StcConnectionListener, IServiceIOListener,
		IStcActivity {

	ArrayList<IFloatingGalleryEventListener> listeners = new ArrayList<IFloatingGalleryEventListener>();
	StcSocket socket;
	ArrayList<StcSocket> sockets = null;
	WriteEngine wengine;
	ReadEngine rengine;
	Bundle initBundle;
	boolean bundleParsed = false;
	UserMode userMode = UserMode.UNDEFINED;

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#onCreate()
	 * 
	 * On creating the service
	 */
	@Override
	public void onCreate() {
		Log.i("fg service", "FloatingGalleryService.onCreate()");
		super.onCreate();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#onDestroy()
	 * 
	 * On destroying the service
	 */
	@Override
	public void onDestroy() {
		Log.i("fg service", "FloatingGalleryService.onDestroy()");
		super.onDestroy();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.app.Service#stopSelf()
	 * 
	 * On exiting the service, stop it
	 */
	public void exitService() {
		Log.i("fg service", "FloatingGalleryService.exitService()");
		stopSelf();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#getAppId()
	 * 
	 * return the App id
	 */
	@Override
	public StcApplicationId getAppId() {
		// TODO Auto-generated method stub
		return FloatingGalleryRegisterApp.id;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#getConnListener()
	 * 
	 * Connection listener fot StcServiceInet
	 */
	@Override
	public StcConnectionListener getConnListener() {
		// TODO Auto-generated method stub
		return this;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#getServiceIntent()
	 * 
	 * return the intent of the core service, which in this case is
	 * FloatingGalleryService
	 */
	@Override
	public String getServiceIntent() {
		// TODO Auto-generated method stub
		return "come.teamarch.fg.FloatingGalleryService";
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.stc.slib.StcServiceInet#stcLibPrepared(com.intel.stc.lib.StcLib
	 * )
	 * 
	 * prepare the stclib object with the objects for the StcConnectionListener
	 * and StcUserListListener Then try to parse the bundle
	 */
	@Override
	protected void stcLibPrepared(StcLib slib) {
		// TODO Auto-generated method stub
		slib.setConnectionListener(this);
		slib.setUserListListener(this);

		tryParseBundle();
	}

	/*
	 * Set the bundle try to parse
	 */
	public void parseInitBundle(Bundle bundle) {
		Log.i("fg service", "FloatingGalleryService.parseInitBundle()");
		initBundle = bundle;
		tryParseBundle();
	}

	/*
	 * Get the stcLib object. If its not found, return. else if bundle is not
	 * parsed, parse it
	 */
	private void tryParseBundle() {
		Log.i("fg service", "FloatingGalleryService.tryParseBundle()");
		StcLib lib = getSTCLib();
		if (lib == null) {
			return;
		}

		synchronized (this) {
			if (bundleParsed)
				return;
			bundleParsed = true;
		}

		lib.parseStartMethod(this, initBundle, this);
	}

	/*
	 * Add a listener to the list of listeners
	 */
	public boolean addListener(IFloatingGalleryEventListener listener) {
		Log.i("fg service", "FloatingGalleryService.addListener()" + "\n"
				+ listener.getClass().getCanonicalName());
		synchronized (listeners) {
			if (listeners.contains(listener)) {
				return false;
			} else {
				Log.i("fg service", "FloatingGalleryService.addListener()" + "\r" + listener.getClass().getCanonicalName());
				return listeners.add(listener);
			}
		}
	}

	/*
	 * Remove a listener from the list of listeners
	 */
	public boolean removeListener(IFloatingGalleryEventListener listener) {
		Log.i("fg service", "FloatingGalleryService.removeListener()");
		synchronized (listeners) {
			return listeners.remove(listener);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.slib.StcServiceInet#stcPlatformMissing()
	 * 
	 * <!-- No idea wat this does -->
	 */
	@Override
	protected void stcPlatformMissing() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.interfaces.IStcActivity#onStartClient(java.util.UUID,
	 * int)
	 * 
	 * The app has been launched because an invitation was accepted
	 */
	@Override
	public void onStartClient(UUID inviterUuid, int inviteHandle) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.interfaces.IStcActivity#onStartNormal()
	 * 
	 * The app is starts normally
	 */
	@Override
	public void onStartNormal() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.intel.stc.interfaces.IStcActivity#onStartServer(java.util.UUID)
	 * 
	 * Invite a user
	 */
	@Override
	public void onStartServer(UUID userUuid) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.stc.interfaces.StcConnectionListener#connectionCompleted(com
	 * .intel.stc.events.InviteResponseEvent)
	 * 
	 * The response to an outgoing invitation is caught here
	 */
	@Override
	public void connectionCompleted(InviteResponseEvent ire) {
		// TODO Auto-generated method stub
		Log.i("fg service", "connectionCompleted().ire : " + ire.toString());
		boolean connectionComplete = false;

		if (ire.getStatus() == InviteResponseEvent.InviteStatus.sqtAccepted) {
			Log.i("fg service", "connection is accepted");
			try {
				socket = getSTCLib().getPreparedSocket(ire.getSessionGuid(),
						ire.getConnectionHandle());
				if (socket != null)
					Log.i("fg service", "got a socket");

				InputStream iStream = socket.getInputStream();
				rengine = new ReadEngine(iStream, this);
				if (rengine != null)
					Log.i("fg service", "got a rengine");

				OutputStream oStream = socket.getOutputStream();
				wengine = new WriteEngine(this, oStream);

				connectionComplete = true;
			} catch (StcException e) {
				Log.e("fg service", e.toString());
			}
		}

		postConnected(connectionComplete);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.stc.interfaces.StcConnectionListener#connectionRequest(com.
	 * intel.stc.events.InviteRequestEvent)
	 * 
	 * Notifies that an invitation is caught from a remote user
	 */
	@Override
	public void connectionRequest(InviteRequestEvent ire) {
		// TODO Auto-generated method stub
		boolean connected = false;
		UUID uuid = ire.getSessionUuid();
		int handle = ire.getConnectionHandle();
		try {
			socket = getSTCLib().acceptInvitation(uuid, handle);

			InputStream iStream = socket.getInputStream();
			rengine = new ReadEngine(iStream, this);

			OutputStream oStream = socket.getOutputStream();
			wengine = new WriteEngine(this, oStream);
			connected = true;
		} catch (StcException e) {
			Log.e("fg service", "Connection failed", e);
		}

		if (connected) {
			Log.i("fg service", "Connection success!");
		} else {
			Log.i("fg service", "Connection failed");
		}
		postConnected(connected);
	}

	private void postConnected(boolean connected) {
		// TODO Auto-generated method stub
		synchronized (listeners) {
			for (IFloatingGalleryEventListener l : listeners)
				l.connected(connected);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.intel.stc.interfaces.StcUserListListener#userListUpdated(com.intel
	 * .stc.events.StcUpdateEvent)
	 * 
	 * Notifies user discovery changes Triggered on 3 conditions : a. new user
	 * comes in proximity b. exisiting user leaves the proximity c. change in
	 * the StcUser object for any user
	 */
	@Override
	public void userListUpdated(StcUpdateEvent arg0) {
		// TODO Auto-generated method stub

	}

	public List<StcUser> getUsers() {
		StcLib lib = getSTCLib();
		if (lib != null) {
			try {
				return lib.getUserListWithAvatar();
			} catch (StcException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return new ArrayList<StcUser>();
	}

	@Override
	public void dataReceived(byte[] header) {
		// TODO Auto-generated method stub
		for (IFloatingGalleryEventListener listener : listeners) {
			listener.dataReceived(header);
			Log.i("fg service", "" + listener.getClass().getCanonicalName()
					+ ".lineReceived()");
		}
	}

	@Override
	public void remoteDisconnect() {
		// TODO Auto-generated method stub

	}

	public boolean sendInvitation(StcUser user) {
		// TODO Auto-generated method stub
		try {
			Log.i("fg service",
					"FloatingGalleryService.sendInvitaion("
							+ user.getSessionUuid() + ")");
			getSTCLib().inviteUser(user.getSessionUuid(), (short) 20);
			return true;
		} catch (StcException e) {
			Log.e("fgService", "invitation failed", e);
		}
		return false;
	}

	public void writeString(String string) {
		// TODO Auto-generated method stub
		wengine.writeString(string);
	}

	public String getLines() {
		// TODO Auto-generated method stub
		return rengine.getLines();
	}

	public byte[] getData(byte[] header) {
		// TODO Auto-generated method stub
		return rengine.getData(header);
	}

	public void requestImage(String str) {
		// TODO Auto-generated method stub
		Log.i("fg temp_gallery", "Forwarding request");
		wengine.requestImage(str);
	}
}
