package com.teamarch.fg;

public interface IFloatingGalleryEventListener {
	void dataReceived(byte[] header);
	void remoteDisconnect();
	void connected(boolean connected);
}
