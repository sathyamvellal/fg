package com.teamarch.fg;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.util.Base64;
import android.util.Log;

public class DataWriter implements Runnable {
	BlockingQueue<Data> queue;
	OutputStream writer;
	byte id = 0;
	Thread thread;

	DataWriter(OutputStream writer) {
		this.queue = new LinkedBlockingQueue<Data>();
		this.writer = writer;
		this.thread = new Thread(this);
		this.thread.start();
	}

	void addText(final String msg) {
		synchronized (queue) {
			id = (byte) ((id + 1) % 256);
			byte[] header = new byte[4];
			header[0] = 100;
			header[1] = id;
			header[2] = 0;
			header[3] = 0;
			int len = msg.length();
			int i = 0; int j = 0;
			String packet = "";
			while (len > 0) {
				j = 1020;
				len -= 1020;
				Log.i("fg data_writer", "writing something");
				if (len < 0) {
					Log.i("fg data_writer", "last packet was written");
					header[2] = 1;
					j += len;
				}
				packet = packet + new String(header) + msg.substring(i, i + j);
				i += 1020;
			}
			Log.i("fg data_writer", "created a packet:");
			Log.i("fg data_writer", "" + header[0]);
			Log.i("fg data_writer", "header[2]" + header[2]);
			byte[] temp = packet.getBytes();
			Log.i("fg data_writer", "temp[0]" + temp[0]);
			Log.i("fg data_writer", "temp[2]" + temp[2]);
			queue.add(new TextData(packet.getBytes()));
		}
	}
	
	void addImage(String path) {
		synchronized (queue) {
			id = (byte) ((id + 1) % 256);
			byte[] header = new byte[4];
			header[0] = 50;
			header[1] = id;
			header[2] = 0;
			header[3] = 0;
			path = "/sdcard/img2.jpg";
			Bitmap bm = BitmapFactory.decodeFile(path);
			bm = Bitmap.createScaledBitmap(bm, 150, 100, true);
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			bm.compress(Bitmap.CompressFormat.JPEG, 60, baos);
//			String msg = Base64ED.encodeBytes(baos.toByteArray());
			queue.add(new ImageData(baos.toByteArray()));
//			int len = msg.length();
//			Log.i("fg data_writer", "Length: " + len);
//			int i = 0; int j = 0;
//			String packet = "";
//			while (len > 0) {
//				j = 0;
//				len -= 1020;
//				Log.i("fg data_writer", "writing something");
//				if (len < 0) {
//					Log.i("fg data_writer", "last packet was written");
//					header[2] = 1;
//					j = len;
//				}
//				j += 1020;
//				packet = packet + new String(header) + msg.substring(i, i + j);
//				i += 1020;
//			}
//			Log.i("fg data_writer", "created an image packet:");
//			Log.i("fg data_writer", "" + header[0]);
//			Log.i("fg data_writer", "" + header[2]);
//			byte[] temp = packet.getBytes();
//			Log.i("fg data_writer", "temp[0]" + temp[0]);
//			Log.i("fg data_writer", "temp[2]" + temp[2]);
//			Log.i("fg data_writer", "packet_length: " + packet.length());
//			queue.add(new ImageData(packet.getBytes()));
		}
	}
	
	public void run() {
		while (true) {
			Data data;
			try {
				data = queue.take();
				Log.i("fg data_writer", "Now writing the data to the socket!");
				writer.write(data.getData());
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	abstract class Data {
		byte[] dataStream;
		char type;

		String getTypeString() {
			String typeString = null;

			switch ((int) type) {
			case 10:
				// master/regular query
				typeString = "mr_query";
				break;
			case 11:
				// master/regular response
				typeString = "mr_response";
				break;
			case 15:
				// user list query
				typeString = "ul_query";
				break;
			case 16:
				// user list response
				typeString = "ul_response";
				break;
			case 20:
				// data length packet
				typeString = "data_length";
				break;
			case 50:
				// text data
				typeString = "text_data";
				break;
			case 99:
				//image request
				typeString = "image_request";
				break;
			case 100:
				// image data
				typeString = "image_data";
				break;
			default:
				return "";
			}

			return typeString;
		}
		
		abstract byte[] getData();
	}

	class TextData extends Data {
		TextData(byte[] msg) {
			this.dataStream = msg;
		}
		
		byte[] getData() {
			return this.dataStream;
		}
	}

	class ImageData extends Data {
		ImageData(byte[] msg) {
			this.dataStream = msg;
		}
		
		byte[] getData() {
			return this.dataStream;
		}
	}

	public void requestImage(String msg) {
		// TODO Auto-generated method stub
		synchronized (queue) {
			Log.i("fg temp_gallery", "Building request image packet");
			id = (byte) ((id + 1) % 256);
			byte[] header = new byte[4];
			header[0] = 99;
			header[1] = id;
			header[2] = 0;
			header[3] = 0;
			int len = msg.length();
			int i = 0; int j = 0;
			String packet = "";
			while (len > 0) {
				j = 1020;
				len -= 1020;
				Log.i("fg data_writer", "writing something");
//				if (len < 0) {
					Log.i("fg data_writer", "last packet was written. Request packet built!");
					header[2] = 1;
					j += len;
//				}
				packet = packet + new String(header) + msg.substring(i, i + j);
				i += 1020;
			}
			Log.i("fg data_writer", "created a packet:");
			Log.i("fg data_writer", "" + header[0]);
			Log.i("fg data_writer", "header[2]" + header[2]);
			byte[] temp = packet.getBytes();
			Log.i("fg data_writer", "temp[0]" + temp[0]);
			Log.i("fg data_writer", "temp[2]" + temp[2]);
			queue.add(new TextData(packet.getBytes()));
		}
	}
}
