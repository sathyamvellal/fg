package com.teamarch.fg;

import java.util.ArrayList;
import java.util.List;
import android.util.Log;
import java.util.UUID;

import android.content.Context;

import com.intel.stc.lib.AppRegisterService;
import com.intel.stc.lib.GadgetRegistration;
import com.intel.stc.utility.StcApplicationId;
import com.intel.stc.utility.StcCloudAppRole;

/*
 * This class is used to register the app with the app register service of the ICC
 */
public class FloatingGalleryRegisterApp extends AppRegisterService {
	public static final String		APP_UUID		= "7A1B397B-B576-44C4-943F-1BEF4F490C05";
	public static final String		LAUNCH_INTENT	= "com.teamarch.fg";

	static final UUID				appUuid			= UUID.fromString(APP_UUID);

	private static final UUID		apiKeyId		= UUID.fromString("DF26B96A-F1A7-4721-959C-760191C446A8");
	private static final String		apiSectret		= "Nq2yl0bPFxQFse6Ts+uqVQuwHWHz98IUrGkflJ3wZRQ=";
	
	private static final String		appTypeID		= "7A1B397B-B576-44C4-943F-1BEF4F490C06";
	private static final String		appTypeName		= "Floating Gallery";

	static final StcApplicationId	id			= new StcApplicationId(appUuid, apiKeyId, apiSectret, appTypeID,
															appTypeName, StcCloudAppRole.BOTH);
	@Override
	protected List<GadgetRegistration> getGadgetList(Context context)
	{
		Log.i("fg register","Entered Registration");
		String appName = context.getString(R.string.app_name);
		String appDescription = context.getString(R.string.app_description);

		ArrayList<GadgetRegistration> list = new ArrayList<GadgetRegistration>();
		list.add(new GadgetRegistration(appName, R.drawable.ic_launcher, APP_UUID, appDescription, LAUNCH_INTENT, 2,
				R.string.schat_inv_text, R.string.timeout_toast_text, 0, context));
		Log.i("fg register","Completed Registration");
		return list;	
	}
}
